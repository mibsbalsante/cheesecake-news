import { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'

import Header from './components/Header'
import Routes from './views/Routes'

import './assets/settings.css'
import './App.css'

export default class App extends Component {
  render = () => (
    <BrowserRouter>
      <div className='app'>
        <Header />
        <Routes />
      </div>
    </BrowserRouter>
  )
}
