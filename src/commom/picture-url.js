const fileExists = async url => {
  const file = await fetch(url)
  return file.status < 400
}

const pictureUrl = () => {
  const prefix = window.innerWidth >= 750 ? '@2x' : ''
  const fn = async url => {
    const file = '/static/img/' + url + prefix
    const pngExists = await fileExists(file + '.png')
    return pngExists ? file + '.png' : file + '.jpg'
  }
  return fn
}

export default pictureUrl
