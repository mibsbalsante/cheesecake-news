import { Component } from 'react'
import { NavLink } from 'react-router-dom'

import Navigation from './Navigation'

import pictureUrl from '../commom/picture-url'

import './Header.css'

class Header extends Component {
  state = {
    logoUrl: '',
    isNavVisible: false
  }

  constructor () {
    super()
    this.logoName = 'logo'
    this.pictureUrl = pictureUrl()
  }

  toggleNavigation = ev => {
    this.setState(oldState => (
      { isNavVisible: !oldState.isNavVisible }
    ))
  }

  componentWillMount = () => {
    if (!this.state.logoUrl) {
      this.pictureUrl(this.logoName).then(pic => {
        this.setState({ logoUrl: pic })
      })
    }
  }

  render = () => {
    return (
      <header className='header'>
        <div className='header_container'>
          <button className='header_toggle_nav'
            onClick={this.toggleNavigation}>
            <span />
          </button>
          <NavLink className='header_title' to='/'>
            <img className='header_logo'
              onClick={this.toggleNavigation}
              src={this.state.logoUrl}
              alt='webpage logo' />
          </NavLink>
          <Navigation toggleNavigation={this.toggleNavigation}
            isNavVisible={this.state.isNavVisible} />
        </div>
      </header>
    )
  }
}

export default Header
