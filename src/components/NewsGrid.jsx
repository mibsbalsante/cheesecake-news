import { Component } from 'react'
import Post from './Post'

import request from '../commom/requests'

import './NewsGrid.css'

class NewsGrid extends Component {
  state = {
    categories: [],
    posts: [],
    error: '',
    loading: false
  }

  getCategories = () => (
    this.state.categories.length === 0 ? []
      : this.props.categories.map(propsCategory => (
        this.state.categories.find(category => (
          category.param === propsCategory
        ))
      )).filter(category => category)
  )

  getPosts = categories => (
    categories.length === 0 ? this.state.posts
      : this.state.posts.filter(post => (
        categories.find(category => category.id === post.tag)
      ))
  )

  getPostsComp = (arr, key) => (
    arr.length === 0
      ? []
      : (<div key={key} className={`grid_${key}`}>
        {arr.map(post => <Post key={post.id} post={post} categories={this.state.categories} />)}
      </div>)
  )

  makeRequest = (url, stateName) => {
    this.setState({ error: '', loading: true })

    request(url)
      .then(dt => dt.json())
      .then(json => { this.setState({ [stateName]: json.data }) })
      .catch(err => { this.setState({ error: err.message }) })
      .finally(() => {
        setTimeout(() => {
          this.setState({ loading: false })
        }, 3000)
      })
  }

  componentWillMount = () => {
    this.makeRequest('news', 'posts')
    this.makeRequest('categories', 'categories')
  }

  render = () => {
    const { error, loading } = this.state
    const categories = this.getCategories()
    const posts = this.getPosts(categories)

    const categoriesText = this.props.categories.join(', ')

    const childLoading = <span className='grid_loading'>Loading</span>
    const childError = <span className='grid_error'>{error}</span>
    const childEmpty = (
      <span className='grid_notfound'>
        There is no news with
        <strong> {categoriesText} </strong>
        tag.
      </span>
    )
    const childPosts = () => {
      const topPosts = []
      const commomPosts = []

      posts.map(post => post.top
        ? topPosts.push(post)
        : commomPosts.push(post)
      )

      return [
        this.getPostsComp(topPosts, 'top'),
        this.getPostsComp(commomPosts, 'commom')
      ]
    }

    const content = loading ? childLoading
      : error ? childError
        : posts.length > 0 ? childPosts()
          : childEmpty

    return <div className='grid'>{content}</div>
  }
}

export default NewsGrid
