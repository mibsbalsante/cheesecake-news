import { Component } from 'react'

import pictureUrl from '../commom/picture-url'

import './Post.css'

class Post extends Component {
  state = {
    pictureUrl: ''
  }

  constructor (props) {
    super(props)
    this.pictureUrl = pictureUrl()
  }

  componentWillMount = () => {
    const { picture } = this.props.post

    if (!this.state.pictureUrl && picture) {
      this.pictureUrl(picture).then(pic => {
        this.setState({ pictureUrl: pic })
      })
    }
  }

  render = () => {
    const { categories, post } = this.props
    const category = categories.find(category => category.id === post.tag)

    return (
      <div className='post'>
        <span className={`post_category -${category.param}`}>{category.param}</span>
        {
          post.picture &&
          <div className='post_picture'>
            <img src={this.state.pictureUrl} alt={post.title} />
            <button className='post_link'>Read More</button>
          </div>
        }
        <h2 className='post_title'>{post.title}</h2>
        <div className='post_author'>
          <span>by {post.author}</span>
        </div>
        <p className='post_text'>{post.text}</p>
      </div>
    )
  }
}

export default Post
