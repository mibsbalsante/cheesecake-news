import { Component } from 'react'
import { NavLink } from 'react-router-dom'

import request from '../commom/requests'

import './Navigation.css'

class Navigation extends Component {
  state = {
    categories: []
  }

  componentWillMount = () => {
    request('categories')
      .then(dt => dt.json())
      .then(json => {
        this.setState({ categories: json.data })
      })
  }

  render = () => {
    const { toggleNavigation } = this.props
    const isVisible = (window.innerWidth < 750 &&
      this.props.isNavVisible)
      ? '-visible'
      : ''
    const { categories } = this.state
    const links = categories.map(category => (
      <NavLink
        className={`navigation_link -${category.param}`}
        to={`/${category.param}`}
        onClick={toggleNavigation}
        key={category.id}>
        {category.param}
      </NavLink>
    ))

    return (
      <nav className={`navigation ${isVisible}`}>
        {links}
        <NavLink className='navigation_link -login'
          onClick={toggleNavigation}
          to='/login'>Login</NavLink>
      </nav>
    )
  }
}

export default Navigation
