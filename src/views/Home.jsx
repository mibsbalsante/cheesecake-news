import { Component } from 'react'
import { Route } from 'react-router-dom'

import NewsGrid from '../components/NewsGrid'

class Home extends Component {
  render = () => {
    const { myInterests } = this.props
    const renderCategory = (props) => <NewsGrid categories={[props.match.params.category]} />
    const renderGrid = () => <NewsGrid categories={myInterests} />

    return (
      <main className='home'>
        <Route path='/:category' render={renderCategory} />
        <Route exact path='/' render={renderGrid} />
      </main>
    )
  }
}

export default Home
