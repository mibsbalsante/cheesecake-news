import { Component } from 'react'

import './Login.css'

class Login extends Component {
  state = {
    user: '',
    pass: ''
  }

  handleFieldValue = ev => {
    const field = ev.target.dataset.field
    const value = ev.target.value
    this.setState({ [field]: value })
  }

  makeLogin = ev => {
    ev.preventDefault()
    this.props.setUsername(this.state.user)
    this.props.historyPush('/interests')
  }

  render = () => (
    <main className='login user_page'>
      <h2 className='user_page_title'>User area</h2>

      <form className='form' onSubmit={this.makeLogin}>
        <div className='form_line'>
          <label className='form_label'>Username</label>
          <input className='form_field'
            value={this.state.user}
            data-field='user'
            onChange={this.handleFieldValue}
            type='text'
            required />
        </div>
        <div className='form_line'>
          <label className='form_label'>Password</label>
          <input className='form_field'
            value={this.state.pass}
            data-field='pass'
            onChange={this.handleFieldValue}
            type='password'
            required />
        </div>
        <button className='form_submit'>Login</button>
      </form>
    </main>
  )
}

export default Login
