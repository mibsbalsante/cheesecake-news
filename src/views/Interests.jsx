import { Component } from 'react'
import { NavLink } from 'react-router-dom'

import request from '../commom/requests'

import './Interests.css'

class Interests extends Component {
  constructor (props) {
    super(props)

    this.state = {
      categories: [],
      selectedCategories: props.selected
    }
  }

  isSelected = id => {
    const hasFound = this.state.selectedCategories.find(sc => (
      sc.id === id
    ))
    return hasFound ? 'selected' : ''
  }

  toggleCategory = selected => {
    const { selectedCategories } = this.state
    const selInd = selectedCategories.reduce((res, item, ind) => (
      selected.id === item.id ? ind : res
    ), -1)
    selInd >= 0
      ? selectedCategories.splice(selInd, 1)
      : selectedCategories.push(selected)

    this.setState({ selectedCategories })
  }

  saveInterests = ev => {
    ev.preventDefault()
    this.props.setInterests(this.state.selectedCategories)
    this.props.historyPush('/')
  }

  componentWillMount = () => {
    request('categories')
      .then(dt => dt.json())
      .then(json => {
        this.setState({ categories: json.data })
      })
  }

  render = () => {
    const selectionList = this.state.categories.map(category => {
      const className = `interests_button
                          -${category.param}
                          ${this.isSelected(category.id)}`
      return (
        <button key={category.id}
          type='button'
          className={className}
          onClick={() => this.toggleCategory(category)}>
          {category.param}
        </button>
      )
    })
    return (
      <main className='interests user_page'>
        <h2 className='user_page_title'>
          Welcome,
          <span className='user_name'> {this.props.username}</span>
        </h2>
        <form className='form' onSubmit={this.saveInterests}>
          <div className='form_line'>
            <label className='form_label'>My interests</label>
            <div className='interests_selection'>{selectionList}</div>
          </div>
          <button className='form_submit'>Save</button>
          <NavLink className='form_back' to='/'>Back to home</NavLink>
        </form>
      </main>
    )
  }
}

export default Interests
