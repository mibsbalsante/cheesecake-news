import { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import Home from './Home'
import Login from './Login'
import Interests from './Interests'

class Routes extends Component {
  state = {
    myInterests: [],
    username: null
  }

  setInterests = interests => {
    this.setState({ myInterests: interests })
  }

  setUsername = username => {
    this.setState({ username: username })
  }

  render = () => {
    const paramsInterests = this.state.myInterests.map(item => item.param)
    const user = this.state.username
    const renderLogin = props => (
      !user
        ? <Login historyPush={props.history.push}
          setUsername={this.setUsername} />
        : <Redirect to='/interests' />
    )
    const renderInterests = props => (
      user
        ? <Interests historyPush={props.history.push}
          setInterests={this.setInterests}
          selected={this.state.myInterests}
          username={this.state.username} />
        : <Redirect to='/login' />
    )
    const renderHome = props => (
      <Home match={props.match}
        myInterests={paramsInterests} />
    )

    return (
      <Switch>
        <Route path='/login' render={renderLogin} />
        <Route path='/interests' render={renderInterests} />
        <Route path='/' render={renderHome} />
      </Switch>
    )
  }
}

export default Routes
