module.exports = {
  options: { output: 'dist' },
  use: [
    '@neutrinojs/standardjs',
    [
      '@neutrinojs/react',
      {
        html: {
          title: 'Cheesecake News',
          links: [
            {
              rel: 'shortcut icon',
              href: './static/favicon.ico'
            },
            {
              rel: 'stylesheet',
              href: 'https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:400,400i'
            }
          ]
        }
      }
    ],
    [
      '@neutrinojs/style-loader',
      {
        loaders: [{
          loader: 'postcss-loader',
          useId: 'postcss'
        }]
      }
    ]
  ]
};
