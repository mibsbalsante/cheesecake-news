# Cheesecake News
A news website

## Instructions

### Run locally
- Install [Yarn](https://yarnpkg.com/pt-BR/docs/install)
- Run `yarn install`
- Run `yarn start`

### Lint
- Run `yarn lint [--fix]`

### Build
- Run `yarn build`
- Serve **/dist** files in a http server

### Heroku
There is a **heroku** branch with needed files to serve project in Heroku. Just merge **master** updates with this branch.

- Create a [Heroku](https://www.heroku.com) account and project
- [Install Heroku](https://devcenter.heroku.com/articles/heroku-cli)

Run:

- `heroku git:remote -a your-project-name`
- `heroku config:set NODE_ENV=production --app your-project-name`
- `git checkout heroku`
- `git merge master`
- `yarn build`
- `git add . && git commit -m 'your message here'`
- `git push heroku heroku:master`
